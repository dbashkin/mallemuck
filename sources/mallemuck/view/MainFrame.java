/* Copyright (c) 2013 Running man community.
 *
 * Licensed under GNU LESSER GENERAL PUBLIC LICENSE, Version 3.0(the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.gnu.org/licenses/gpl-3.0.html
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mallemuck.view;

import java.awt.BorderLayout;
import javax.swing.JFrame;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import mallemuck.MallemuckPanel;
import mallemuck.Mallemuck;
import mallemuck.model.Resourses;

/**
 * Encapsulates main frame of the application.
 */
public class MainFrame extends JFrame {
        /**
         * Creates new instance of <code>MainFrame</code>.
         */
        public MainFrame() {
                super();
                initialisation();
        }

        private void initialisation() {
                this.setLookandFeel();
                this.setTitle(Mallemuck.TITLE);
                this.setLogo();
                this.setExtendedState(JFrame.MAXIMIZED_BOTH);
                this.setLayout(new BorderLayout());
                this.add(new MallemuckPanel(), BorderLayout.CENTER);
                this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        }

        private void setLogo() {
                Resourses resourses = new Resourses();
                setIconImage(resourses.getLogo());
        }

        private void setLookandFeel() {
                try {
                        for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels())  {
                                if ("Nimbus".equals(info.getName())) {
                                        UIManager.setLookAndFeel(info.getClassName());
                                        break;
                                }
                        }
                } catch (ClassNotFoundException exception) {
                        exception.printStackTrace();
                } catch (InstantiationException exception) {
                        exception.printStackTrace();
                } catch (IllegalAccessException exception) {
                        exception.printStackTrace();
                } catch (javax.swing.UnsupportedLookAndFeelException exception) {
                        exception.printStackTrace();
                }
        }
}