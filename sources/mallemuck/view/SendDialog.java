package mallemuck.view;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.Rectangle;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import mallemuck.controller.SendDialogController;
import mallemuck.model.Button;
import org.runningman.mailutils.Credentials;
import org.runningman.mailutils.MessageData;

/**
 *
 */
public class SendDialog extends JDialog {

        private JTextField sendTo;
        private JTextField user;
        private JTextField host;
        private JPasswordField password;
        private Container parent;
        private FileListPanel[] panels;

        public SendDialog(Container parent, FileListPanel[] panels) {
                super();
                this.parent = parent;
                this.panels = panels;
                initialisation();
        }

        private void initialisation() {
                this.setTitle("Enter credentials");
                Container contentPane = this.getContentPane();
                this.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

                Component gap = Box.createRigidArea(new Dimension(0, 5));
                this.add(gap);

                JPanel panel = new JPanel();
                GridLayout layout = new GridLayout(4, 2);
                layout.setHgap(5);
                layout.setVgap(5);
                panel.setLayout(layout);

                JLabel toLabel = new JLabel("Send to:");
                toLabel.setHorizontalAlignment(JLabel.TRAILING);
                panel.add(toLabel);
                this.sendTo = new JTextField(15);
                panel.add(this.sendTo);

                JLabel fromLabel = new JLabel("From (user name):");
                fromLabel.setHorizontalAlignment(JLabel.TRAILING);
                panel.add(fromLabel);
                this.user = new JTextField(15);
                panel.add(this.user);

                JLabel hostLabel = new JLabel("Mail host:");
                hostLabel.setHorizontalAlignment(JLabel.TRAILING);
                panel.add(hostLabel);
                this.host = new JTextField(15);
                panel.add(this.host);

                JLabel passwordLabel = new JLabel("Password:");
                passwordLabel.setHorizontalAlignment(JLabel.TRAILING);
                panel.add(passwordLabel);
                this.password = new JPasswordField(15);
                panel.add(this.password);
                this.add(panel);

                JPanel buttons = new JPanel();
                SendDialogController listener = new SendDialogController(this);
                listener.setPanels(this.panels);
                JButton send = new JButton("Send");
                send.setActionCommand(Button.SEND);
                send.addActionListener(listener);
                buttons.add(send);
                JButton cansel = new JButton("Cansel");
                cansel.setActionCommand(Button.CANSEL);
                cansel.addActionListener(listener);
                buttons.add(cansel);
                this.add(buttons);

                this.pack();
                Point center = getCenter(this);
                this.setLocation(center);
                this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                this.setModal(true);
                this.setVisible(true);
        }

        public Credentials getCredentials() {
                Credentials credentials = new Credentials();
                credentials.setUser(this.user.getText());
                String text = new String(this.password.getPassword());
                credentials.setPassword(text);
                credentials.setHost(this.host.getText());
                return credentials;
        }

        public MessageData getMessageData() {
                MessageData messageData = new MessageData();
                messageData.setRecepients(this.sendTo.getText());
                return messageData;
        }

        private Point getCenter(JDialog dialog) {
                Rectangle parentBounds = this.parent.getBounds();
                Dimension size = dialog.getSize();
                int x = Math.max(0, parentBounds.x + (parentBounds.width - size.width) / 2);
                int y = Math.max(0, parentBounds.y + (parentBounds.height - size.height) / 2);
                return new Point(x, y);
        }
}