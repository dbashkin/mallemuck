/* Copyright (c) 2011 Running man community.
 *
 * Licensed under GNU LESSER GENERAL PUBLIC LICENSE, Version 3.0(the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.gnu.org/licenses/gpl-3.0.html
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mallemuck.view;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.JPanel;
import mallemuck.controller.CopyController;
import mallemuck.controller.CreateFileController;
import mallemuck.controller.DeleteController;
import mallemuck.controller.EditController;
import mallemuck.controller.MakeDirController;
import mallemuck.controller.MoveController;
import mallemuck.controller.RenameController;
import mallemuck.controller.SendController;
import mallemuck.controller.ViewController;
import mallemuck.controller.ZipController;
import mallemuck.model.Resourses;

/**
 * Encapsulates button Tool Bar.
 */
public class ButtonToolBar extends JPanel {
        public ButtonToolBar(FileListPanel leftPanel, FileListPanel rightPanel) {
                Resourses resources = new Resourses();
                Font font = resources.getDefaultFont();
                // 'Send' button.
                JButton sendButton = new JButton("Send");
                sendButton.setActionCommand(sendButton.getText());
                SendController sendController = new SendController();
                sendController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                sendButton.addActionListener(sendController);
                sendButton.setFont(font);
                this.add(sendButton);
                // 'Create file' button.
                JButton createFileButton = new JButton("Create file");
                createFileButton.setActionCommand(createFileButton.getText());
                CreateFileController createFileController = new CreateFileController();
                createFileController.setPanels(
                        new FileListPanel[]{leftPanel, rightPanel});
                createFileButton.addActionListener(createFileController);
                createFileButton.setFont(font);
                this.add(createFileButton);
                // 'Rename' button.
                JButton renameButton = new JButton("Rename");
                renameButton.setActionCommand(renameButton.getText());
                RenameController renameController = new RenameController();
                renameController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                renameButton.addActionListener(renameController);
                renameButton.setFont(font);
                this.add(renameButton);
                // 'View' button.
                JButton viewButton = new JButton("View");
                viewButton.setActionCommand(viewButton.getText());
                ViewController viewController = new ViewController();
                viewController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                viewButton.addActionListener(viewController);
                viewButton.setFont(font);
                this.add(viewButton);
                // 'Edit' button.
                JButton editButton = new JButton("Edit");
                editButton.setActionCommand(editButton.getText());
                EditController editController = new EditController();
                editController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                editButton.addActionListener(editController);
                editButton.setFont(font);
                this.add(editButton);
                // 'Copy' button.
                JButton copyButton = new JButton("Copy");
                copyButton.setActionCommand(copyButton.getText());
                CopyController copyController = new CopyController();
                copyController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                copyButton.addActionListener(copyController);
                copyButton.setFont(font);
                this.add(copyButton);
                // 'Move' button.
                JButton moveButton = new JButton("Move");
                moveButton.setActionCommand(moveButton.getText());
                MoveController moveController = new MoveController();
                moveController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                moveButton.addActionListener(moveController);
                moveButton.setFont(font);
                this.add(moveButton);
                // 'Make directory' button.
                JButton makeDirectoryButton = new JButton("Make directory");
                makeDirectoryButton.setActionCommand(makeDirectoryButton.getText());
                MakeDirController controller = new MakeDirController();
                controller.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                makeDirectoryButton.addActionListener(controller);
                makeDirectoryButton.setFont(font);
                this.add(makeDirectoryButton);
                // 'Delete' button.
                JButton deleteButton = new JButton("Delete");
                deleteButton.setActionCommand(deleteButton.getText());
                DeleteController deleteController = new DeleteController();
                deleteController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                deleteButton.addActionListener(deleteController);
                deleteButton.setFont(font);
                this.add(deleteButton);
                // 'Zip/Unzip' button.
                JButton zipButton = new JButton("Zip/Unzip");
                zipButton.setActionCommand(zipButton.getText());
                ZipController zipController = new ZipController();
                zipController.setPanels(new FileListPanel[]{leftPanel, rightPanel});
                zipController.setButtonZip(zipButton);
                zipButton.addActionListener(zipController);
                zipButton.setFont(font);
                this.add(zipButton);
        }
}