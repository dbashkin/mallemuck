package mallemuck.view;

import audiovideoplayer.model.AudioPlayer;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.border.BevelBorder;
import mallemuck.model.ListFilter;
import mallemuck.model.Resourses;

/**
 * Encapsulates panel with list of files.
 */
public class FileListPanel extends JPanel {
        private File root;
        private JList fileList;
        private JLabel rootPath;
        public FileListPanel() {
                this.setLayout(new BorderLayout());
                JPanel navigationToolBar = createNavigationToolBar();
                JPanel pathToolBar = createPathToolBar();
                JPanel fileListPanel = createFileListPanel();
                Box box = Box.createVerticalBox();
                box.add(navigationToolBar);
                box.add(pathToolBar);
                this.add(box, BorderLayout.PAGE_START);
                this.add(fileListPanel, BorderLayout.CENTER);
        }
        public File getCurrentPath() {
                return root;
        }
        public boolean isSelected() {
                int selected = fileList.getSelectedIndex();
                return selected != -1;
        }
        public String getSelected() {
                return fileList.getSelectedValue().toString();
        }
        public File getSelectedFile() {
                return (File) fileList.getSelectedValue();
        }
        public Collection<File> getSelectedFiles() {
                List<File> selected = fileList.getSelectedValuesList();
                return selected;
        }
        public void refresh() {
                File current = getCurrentPath();
                setListData(current);
        }
        private JPanel createNavigationToolBar() {
                JPanel panel = new JPanel();
                Resourses resources = new Resourses();
                Font font = resources.getDefaultFont();
                JButton buttonHome = new JButton("Home");
                buttonHome.setFont(font);
                buttonHome.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent event) {
                                File home = getHome();
                                setListData(home);
                        }
                });
                panel.add(buttonHome);
                JButton buttonUp = new JButton("Up");
                buttonUp.setFont(font);
                buttonUp.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent event) {
                                if (null != root.getParentFile()) {
                                        File parent = root.getParentFile();
                                        setListData(parent);
                                }
                        }
                });
                panel.add(buttonUp);
                return panel;
        }
        private JPanel createPathToolBar() {
                JPanel panel = new JPanel();
                rootPath = new JLabel();
                Resourses resources = new Resourses();
                Font font = resources.getDefaultFont();
                rootPath.setFont(font);
                panel.add(rootPath);
                Dimension maximumSize = new Dimension(400, 20);
                panel.setMaximumSize(maximumSize);
                return panel;
        }
        private JPanel createFileListPanel() {
                JPanel panel = new JPanel();
                Resourses resources = new Resourses();
                Font font = resources.getDefaultFont();
                fileList = new JList();
                fileList.setBackground(new Color(190, 255, 190));
                fileList.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED));
                fileList.setFont(font);
                File home = getHome();
                setListData(home);
                fileList.addMouseListener(new MouseAdapter() {
                        public void mouseClicked(MouseEvent event) {
                                if (event.getClickCount() == 2) {
                                        File selected = (File) fileList.getSelectedValue();
                                        if (selected.isDirectory()) {
                                                setListData(selected);
                                        }
                                        if (selected.getName().endsWith(".mp3")) {
                                                new AudioPlayer(selected).play();
                                        }
                                }
                        }
                        public void mousePressed(MouseEvent event) {
                                if (MouseEvent.BUTTON2 == event.getButton()) {
                                }
                        }
                });
                int w = Toolkit.getDefaultToolkit().getScreenSize().width / 2 - 4;
                fileList.setPreferredSize(new Dimension(w, 400));
                fileList.setCellRenderer(new DefaultListCellRenderer() {
                        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
                                File file = (File) value;
                                String name = file.getName();
                                return super.getListCellRendererComponent(list, name, index, isSelected, cellHasFocus);
                        }
                });
                panel.setLayout(new BorderLayout());
                panel.add(fileList, BorderLayout.CENTER);
                return panel;
        }
        private void setListData(File directory) {
                root = directory;
                rootPath.setText(root.getAbsolutePath());
                File[] listData = root.listFiles(new ListFilter());
                Arrays.sort(listData);
                fileList.setListData(listData);
        }
        private File getHome() {
                String path = System.getProperty("user.home");
                File file = new File(path);
                return file;
        }
}