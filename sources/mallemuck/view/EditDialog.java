package mallemuck.view;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.JDialog;
import jeditor.JEditorPanel;

/**
 * Encapsulates editor.
 */
public class EditDialog extends JDialog {
        public EditDialog(File file) {
                this.setModal(true);
                Container container = this.getContentPane();
                JEditorPanel jeditor = new JEditorPanel();
                jeditor.setFile(file);
                container.add(jeditor, BorderLayout.CENTER);
                this.expand();
                this.setVisible(true);
        }

        private void expand() {
                Dimension size = Toolkit.getDefaultToolkit().getScreenSize();
                this.setSize(size);
        }
}