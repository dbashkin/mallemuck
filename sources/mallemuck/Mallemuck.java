/* Copyright (c) 2011 Running man community.
 *
 * Licensed under GNU LESSER GENERAL PUBLIC LICENSE, Version 3.0(the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.gnu.org/licenses/gpl-3.0.html
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mallemuck;

import javax.swing.SwingUtilities;
import mallemuck.view.MainFrame;

/**
 * Main class of the application.
 */
public class Mallemuck {
        public static final String TITLE = "Альбатрос-ТБ-7";
        /**
         * Entry point of the program.
         * @param arguments the command line arguments.
         */
        public static void main(String[] arguments) {
                final MainFrame mainFrame = new MainFrame();
                SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                                mainFrame.setVisible(true);
                        }
                });
        }
}