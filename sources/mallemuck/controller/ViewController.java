package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import mallemuck.view.FileListPanel;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Keys;
import mallemuck.model.command.View;

/**
 *
 */
public class ViewController implements ActionListener {
        private FileListPanel[] panels;
        public ViewController() {
        }
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        @Override
        public void actionPerformed(ActionEvent event) {
                // Get directory name.
                File file = getSelectedFile();
                CommandData data = new CommandData();
                data.addItem(Keys.FILE, file);
                // Create command.
                View view = new View();
                try {
                        view.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
        }
        private File getSelectedFile() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                File result = panel.getSelectedFile();
                                return result;
                        }
                }
                return null;
        }
}