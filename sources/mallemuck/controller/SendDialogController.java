package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import javax.swing.JOptionPane;
import mallemuck.model.Button;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Keys;
import mallemuck.model.command.SendEmail;
import mallemuck.view.FileListPanel;
import mallemuck.view.SendDialog;
import org.runningman.mailutils.Credentials;
import org.runningman.mailutils.MessageData;

/**
 *
 */
public class SendDialogController implements ActionListener {
        private SendDialog dialog;
        private FileListPanel[] panels;
        public SendDialogController(SendDialog dialog) {
                this.dialog = dialog;
        }
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        public void actionPerformed(ActionEvent event) {
                if (Button.CANSEL.equals(event.getActionCommand())) {
                        dialog.dispose();
                        return;
                }
                CommandData data = new CommandData();
                Credentials credentials = dialog.getCredentials();
                data.addItem(Keys.CREDENTIALS, credentials);
                Collection<File> sources = getSourceFiles();
                data.addItem(Keys.SOURCE_FILES, sources);
                MessageData messageData = dialog.getMessageData();
                data.addItem(Keys.MESSAGE_DATA, messageData);
                // Create command.
                SendEmail sendEmail = new SendEmail();
                try {
                        sendEmail.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                        JOptionPane.showMessageDialog(
                                dialog,
                                "Сообщение не было отправлено.",
                                "Error message.",
                                JOptionPane.ERROR_MESSAGE);
                }
        }
        private Collection<File> getSourceFiles() {
                FileListPanel panel = getSourcePanel();
                Collection<File> sources = panel.getSelectedFiles();
                return sources;
        }
        private FileListPanel getSourcePanel() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                return panel;
                        }
                }
                return null;
        }
}