package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import mallemuck.view.FileListPanel;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Delete;
import mallemuck.model.command.Keys;
import mallemuck.model.command.Refresh;

/**
 *
 */
public class DeleteController implements ActionListener {
        private FileListPanel[] panels;
        public DeleteController() {
        }
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        public void actionPerformed(ActionEvent event) {
                // Get directory name.
                String path = getSelectedPath();
                CommandData data = new CommandData();
                data.addItem(Keys.DIR_NAME, path);
                // Create command.
                Delete delete = new Delete();
                try {
                        delete.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
                // Refresh panels.
                CommandData refreshData = new CommandData();
                refreshData.addItem(Keys.PANELS, panels);
                Refresh refresh = new Refresh();
                try {
                        refresh.perform(refreshData);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
        }
        private String getSelectedPath() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                String result = panel.getSelected();
                                return result;
                        }
                }
                return null;
        }
}