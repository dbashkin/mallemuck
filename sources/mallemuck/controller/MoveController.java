package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Keys;
import mallemuck.model.command.Move;
import mallemuck.model.command.Refresh;
import mallemuck.view.FileListPanel;

/**
 *
 */
public class MoveController implements ActionListener {

    private FileListPanel[] panels;

    public MoveController() {}

    public void setPanels(FileListPanel[] panels) {
        this.panels = panels;
    }

    @Override
    public void actionPerformed(ActionEvent event) {
        // Get directory name.
        Collection<File> sources = getSourceFiles();
        File destanation = getDestanationPath();
        CommandData data = new CommandData();
        data.addItem(Keys.SOURCE_FILES, sources);
        data.addItem(Keys.DEST_DIR, destanation);
        // Create command.
        Move move = new Move();
        try {
            move.perform(data);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        // Refresh panels.
        CommandData refreshData = new CommandData();
        refreshData.addItem(Keys.PANELS, panels);
        Refresh refresh = new Refresh();
        try {
            refresh.perform(refreshData);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private Collection<File> getSourceFiles() {
        FileListPanel panel = getSourcePanel();
        Collection<File> sources = panel.getSelectedFiles();
        return sources;
    }

    private File getDestanationPath() {
        for(FileListPanel panel : panels) {
            if (!panel.isSelected()) {
                return panel.getCurrentPath();
            }
        }
        return null;
    }

    private FileListPanel getSourcePanel() {
        for(FileListPanel panel : panels) {
            if (panel.isSelected()) {
                return panel;
            }
        }
        return null;
    }
}
