package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JOptionPane;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.CreateFile;
import mallemuck.model.command.Keys;
import mallemuck.model.command.Refresh;
import mallemuck.view.FileListPanel;

/**
 *
 */
public class CreateFileController implements ActionListener {

    private FileListPanel[] panels;

    public CreateFileController() {}

    public void setPanels(FileListPanel[] panels) {
        this.panels = panels;
    }

    public void actionPerformed(ActionEvent event) {
        // Get file name.
        String name = JOptionPane.showInputDialog("Enter file name:");
        CommandData data = new CommandData();
        String path = getAbsolutePath() + "/" + name;
        File file = new File(path);
        data.addItem(Keys.FILE, file);
        // Create command.
        CreateFile createFile = new CreateFile();
        try {
            createFile.perform(data);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        // Refresh panels.
        CommandData refreshData = new CommandData();
        refreshData.addItem(Keys.PANELS, panels);
        Refresh refresh = new Refresh();
        try {
            refresh.perform(refreshData);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    private String getAbsolutePath() {
        for(FileListPanel panel : panels) {
            if (panel.isSelected()) {
                return panel.getCurrentPath().getAbsolutePath();
            }
        }
        return null;
    }
}
