package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import javax.swing.JOptionPane;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Keys;
import mallemuck.model.command.Refresh;
import mallemuck.model.command.Rename;
import mallemuck.view.FileListPanel;

/**
 *
 */
public class RenameController implements ActionListener {
        private FileListPanel[] panels;
        public RenameController() {
        }
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        @Override
        public void actionPerformed(ActionEvent event) {
                // Get new name.
                String newName = JOptionPane.showInputDialog("Enter new name:");
                CommandData data = new CommandData();
                data.addItem(Keys.NEW_NAME, newName);
                // Get file to be renamed.
                File file = getSelectedFile();
                data.addItem(Keys.FILE, file);
                // Create command.
                Rename rename = new Rename();
                try {
                        rename.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
                // Refresh panels.
                CommandData refreshData = new CommandData();
                refreshData.addItem(Keys.PANELS, panels);
                Refresh refresh = new Refresh();
                try {
                        refresh.perform(refreshData);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
        }
        private File getSelectedFile() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                File result = panel.getSelectedFile();
                                return result;
                        }
                }
                return null;
        }
}