/* Copyright (c) 2014 Running man community.
 *
 * Licensed under GNU LESSER GENERAL PUBLIC LICENSE, Version 3.0(the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.gnu.org/licenses/gpl-3.0.html
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Collection;
import javax.swing.JButton;
import javax.swing.JDialog;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Keys;
import mallemuck.model.command.Refresh;
import mallemuck.model.command.ZipUnzip;
import mallemuck.view.FileListPanel;
import zipunzip.ZipUnzipPanel;

/**
 *
 */
public class ZipController implements ActionListener {
        private FileListPanel[] panels;
        private JButton zipButton;
                
        public ZipController() {}
        
        public void setButtonZip(JButton button) {
            this.zipButton = button;
        }
                
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        
        public void actionPerformed(ActionEvent event) {
                // Get directory name.
                Collection<File> sources = getSourceFiles();
                File destination = getDestinationPath();
                CommandData data = new CommandData();
                data.addItem(Keys.SOURCE_FILES, sources);
                data.addItem(Keys.DEST_DIR, destination);
                JDialog dialog = new JDialog();
                ZipUnzipPanel panel = new ZipUnzipPanel();                    
                panel.setSource(sources);
                panel.setDestination(destination);
                dialog.add(panel);
                dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
                dialog.pack();
                dialog.setVisible(true);                
                // Create command.
                ZipUnzip command = new ZipUnzip();
                try {
                        command.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
                // Refresh panels.
                CommandData refreshData = new CommandData();
                refreshData.addItem(Keys.PANELS, panels);
                Refresh refresh = new Refresh();
                try {
                        refresh.perform(refreshData);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
        }
        
        private Collection<File> getSourceFiles() {
                FileListPanel panel = getSourcePanel();
                Collection<File> sources = panel.getSelectedFiles();
                return sources;
        }
        
        private File getDestinationPath() {
                for (FileListPanel panel : panels) {
                        if (!panel.isSelected()) {
                                return panel.getCurrentPath();
                        }
                }
                return null;
        }
        
        private FileListPanel getSourcePanel() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                return panel;
                        }
                }
                return null;
        }
}
