package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import mallemuck.view.FileListPanel;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Edit;
import mallemuck.model.command.Keys;

/**
 *
 */
public class EditController implements ActionListener {
        private FileListPanel[] panels;
        public EditController() {
        }
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        public void actionPerformed(ActionEvent event) {
                // Get directory name.
                File file = getSelectedFile();
                CommandData data = new CommandData();
                data.addItem(Keys.FILE, file);
                // Create command.
                Edit edit = new Edit();
                try {
                        edit.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
        }
        private File getSelectedFile() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                File result = panel.getSelectedFile();
                                return result;
                        }
                }
                return null;
        }
}