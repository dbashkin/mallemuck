package mallemuck.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import mallemuck.model.command.CommandData;
import mallemuck.model.command.Keys;
import mallemuck.model.command.MakeDirectory;
import mallemuck.model.command.Refresh;
import mallemuck.view.FileListPanel;

/**
 *
 */
public class MakeDirController implements ActionListener {
        private FileListPanel[] panels;
        public MakeDirController() {
        }
        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }
        public void actionPerformed(ActionEvent event) {
                // Get directory name.
                String name = JOptionPane.showInputDialog("Enter directory name:");
                CommandData data = new CommandData();
                String path = getAbsolutePath() + "/" + name;
                data.addItem(Keys.DIR_NAME, path);
                // Create directory.
                MakeDirectory makeDirectory = new MakeDirectory();
                try {
                        makeDirectory.perform(data);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
                // Refresh panels.
                CommandData refreshData = new CommandData();
                refreshData.addItem(Keys.PANELS, panels);
                Refresh refresh = new Refresh();
                try {
                        refresh.perform(refreshData);
                } catch (Exception exception) {
                        exception.printStackTrace();
                }
        }
        private String getAbsolutePath() {
                for (FileListPanel panel : panels) {
                        if (panel.isSelected()) {
                                return panel.getCurrentPath().getAbsolutePath();
                        }
                }
                return null;
        }
}