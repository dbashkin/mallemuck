package mallemuck.controller;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JPanel;
import mallemuck.view.FileListPanel;
import mallemuck.view.SendDialog;

/**
 *
 */
public class SendController implements ActionListener {

        private FileListPanel[] panels;

        public SendController() {}

        public void setPanels(FileListPanel[] panels) {
                this.panels = panels;
        }

        public void actionPerformed(ActionEvent event) {
                Container parent = getRoot();
                SendDialog dialog = new SendDialog(parent, this.panels);
        }

        private Container getRoot() {
                JPanel panel = panels[0];
                Container root = panel.getRootPane();
                return root;
        }
}
