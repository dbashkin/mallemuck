/* Copyright (c) 2011 Running man community.
 *
 * Licensed under GNU LESSER GENERAL PUBLIC LICENSE, Version 3.0(the "License").
 * You may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *      http://www.gnu.org/licenses/gpl-3.0.html
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mallemuck;

import java.awt.BorderLayout;
import javax.swing.Box;
import javax.swing.JPanel;
import mallemuck.view.ButtonToolBar;
import mallemuck.view.FileListPanel;

/**
 *
 */
public class MallemuckPanel extends JPanel {
        public MallemuckPanel() {
                this.setLayout(new BorderLayout());
                this.setName(Mallemuck.TITLE);
                FileListPanel leftPanel = new FileListPanel();
                FileListPanel rightPanel = new FileListPanel();
                Box box = Box.createHorizontalBox();
                box.add(leftPanel);
                box.add(rightPanel);
                this.add(box, BorderLayout.CENTER);
                ButtonToolBar buttons = new ButtonToolBar(leftPanel, rightPanel);
                this.add(buttons, BorderLayout.SOUTH);
        }
}