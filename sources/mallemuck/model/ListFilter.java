package mallemuck.model;

import java.io.File;
import java.io.FileFilter;

/**
 *
 */
public class ListFilter implements FileFilter {
        public boolean accept(File file) {
                boolean result = !file.isHidden();
                return result;
        }
}