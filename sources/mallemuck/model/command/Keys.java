package mallemuck.model.command;

/**
 * Contains keys to command data.
 */
public interface Keys {
        String SOURCE_FILES = "source_files";
        String DEST_DIR = "dest_dir";
        String DIR_NAME = "dir_name";
        String PANELS = "panels";
        String FILE = "file";
        String NEW_NAME = "new_name";
        String CREDENTIALS = "credentials";
        String MESSAGE_DATA = "message_data";
        String SAVE_DATA = "save";
}
