package mallemuck.model.command;

import java.io.File;
import java.util.Collection;
import org.apache.commons.io.FileUtils;

/**
 *
 */
public class Move implements Command {
        public void perform(CommandData data) throws Exception {
                Collection<File> files =
                        (Collection<File>) data.getValue(Keys.SOURCE_FILES);
                File dir = (File) data.getValue(Keys.DEST_DIR);
                for (File file : files) {
                        FileUtils.moveToDirectory(file, dir, false);
                }
        }
        public void undo() {
        }
}