package mallemuck.model.command;

import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 *
 */
public class Save implements Command {
        public void perform(CommandData data) throws Exception {
                File file = (File) data.getValue(Keys.FILE);
                String saveData = (String) data.getValue(Keys.SAVE_DATA);
                FileUtils.write(file, saveData);
        }
        public void undo() {
                throw new UnsupportedOperationException("Not supported yet.");
        }
}