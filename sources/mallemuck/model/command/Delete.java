package mallemuck.model.command;

import java.io.File;
import org.apache.commons.io.FileUtils;

/**
 *
 */
public class Delete implements Command {

    public void perform(CommandData data) throws Exception {
        String name = (String)data.getValue(Keys.DIR_NAME);
        File file = new File(name);
        FileUtils.forceDelete(file);
    }

    public void undo() {
        throw new UnsupportedOperationException("Not supported yet.");
    }
}
