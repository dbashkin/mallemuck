package mallemuck.model.command;

import java.util.HashMap;

/**
 *
 */
public class CommandData {
        private HashMap<String, Object> data = new HashMap<String, Object>();
        public void addItem(String key, Object value) {
                data.put(key, value);
        }
        public Object getValue(String key) throws UnknownKeyException {
                if (!data.containsKey(key)) {
                        throw new UnknownKeyException(key);
                }
                return data.get(key);
        }
}