package mallemuck.model.command;

import java.io.File;

/**
 *
 */
public class Rename implements Command {
        public void perform(CommandData data) throws Exception {
                File file = (File) data.getValue(Keys.FILE);
                String newName = (String) data.getValue(Keys.NEW_NAME);
                String parent = file.getParent();
                File newFile = new File(parent, newName);
                file.renameTo(newFile);
        }
        public void undo() {
        }
}