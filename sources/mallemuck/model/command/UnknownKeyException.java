package mallemuck.model.command;

/**
 *
 */
public class UnknownKeyException extends Exception {
        private static final String MESSAGE = "Key not found: ";
        public UnknownKeyException(String key) {
                super(MESSAGE + key);
        }
}