package mallemuck.model.command;

/**
 *
 */
public class UnknownCommandException extends Exception {
        private static final String MESSAGE = "Command not found: ";
        public UnknownCommandException(String key) {
                super(MESSAGE + key);
        }
}