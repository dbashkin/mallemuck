package mallemuck.model.command;

import java.io.File;
import java.util.Collection;
import org.apache.commons.io.FileUtils;

/**
 * Performs copiing files and directories.
 */
public class Copy implements Command {
        public void perform(CommandData data) throws Exception {
                Collection<File> files =
                        (Collection<File>) data.getValue(Keys.SOURCE_FILES);
                File directory = (File) data.getValue(Keys.DEST_DIR);
                for (File file : files) {
                        if (file.isFile()) {
                                FileUtils.copyFileToDirectory(file, directory, false);
                        } else {
                                FileUtils.copyDirectoryToDirectory(file, directory);
                        }
                }
        }
        public void undo() {
        }
}