package mallemuck.model.command;

import java.io.File;

/**
 *
 */
public class CreateFile implements Command {
        public void perform(CommandData data) throws Exception {
                File file = (File) data.getValue(Keys.FILE);
                file.createNewFile();
        }
        public void undo() {
                throw new UnsupportedOperationException("Not supported yet.");
        }
}
