package mallemuck.model.command;

/**
 *
 */
public class CommandFactory {

    public CommandFactory() {
    }

    public Command createCommand(String name) throws UnknownCommandException {
        String packageName = Command.class.getPackage().getName();
        String className = packageName + "." + name;
        Class type = null;
        try {
            type = Class.forName(className);
        } catch (ClassNotFoundException exception) {
            exception.printStackTrace();
        }
        Command command = null;
        try {
            command = (Command)type.newInstance();
        } catch (InstantiationException exception) {
            exception.printStackTrace();
        } catch (IllegalAccessException exception) {
            exception.printStackTrace();
        }
        return command;
    }
}
