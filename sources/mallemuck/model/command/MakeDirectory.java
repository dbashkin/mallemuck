package mallemuck.model.command;

import java.io.File;

/**
 *
 */
public class MakeDirectory implements Command {
        public void perform(CommandData data) throws Exception {
                String name = (String) data.getValue(Keys.DIR_NAME);
                File file = new File(name);
                file.mkdir();
        }
        public void undo() {
                throw new UnsupportedOperationException("Not supported yet.");
        }
}