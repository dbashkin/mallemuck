package mallemuck.model.command;

import mallemuck.view.FileListPanel;

/**
 *
 */
public class Refresh implements Command {
        public void perform(CommandData data) throws Exception {
                FileListPanel[] panels = (FileListPanel[]) data.getValue(Keys.PANELS);
                for (FileListPanel panel : panels) {
                        panel.refresh();
                }
        }
        public void undo() {
                throw new UnsupportedOperationException("Not supported yet.");
        }
}