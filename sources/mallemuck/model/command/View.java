package mallemuck.model.command;

import alphaline.AlphalinePanel;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.io.FileInputStream;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.text.rtf.RTFEditorKit;
import jviewer.JViewerPanel;
import org.apache.commons.io.FileUtils;
import org.icepdf.ri.common.ComponentKeyBinding;
import org.icepdf.ri.common.SwingController;
import org.icepdf.ri.common.SwingViewBuilder;

/**
 *
 */
public class View implements Command {

        public void perform(CommandData data) throws Exception {
                File file = (File) data.getValue(Keys.FILE);
                if (file.isDirectory()) {
                        return;
                }
                JDialog dialog = new JDialog();
                dialog.setTitle("View " + file.getName());
                String path = file.getAbsolutePath();
                path = path.toLowerCase();
                FileTypeVerifier verifier = new FileTypeVerifier();
                if (verifier.isText(path)) {
                        JViewerPanel jviewer = new JViewerPanel();
                        String text = FileUtils.readFileToString(file);
                        jviewer.update(text);
                        dialog.add(jviewer);
                } else if (verifier.isRTF(path)) {
                        RTFEditorKit rtf = new RTFEditorKit();
                        JEditorPane editor = new JEditorPane();
                        editor.setEditable(false);
                        editor.setEditorKit(rtf);
                        JScrollPane scroller = new JScrollPane();
                        scroller.getViewport().add(editor);
                        dialog.add(scroller);
                        try {
                                FileInputStream fi = new FileInputStream(file);
                                rtf.read(fi, editor.getDocument(), 0);
                        } catch (Exception exception) {
                                exception.printStackTrace();
                        }
                } else if (verifier.isPicture(path)) {
                        AlphalinePanel viewer = new AlphalinePanel();
                        viewer.setFile(file);
                        dialog.add(viewer);
                } else if (verifier.isPDF(path)) {
                        SwingController controller = new SwingController();
                        SwingViewBuilder factory = new SwingViewBuilder(controller);
                        JPanel viewerComponentPanel = factory.buildViewerPanel();
                        ComponentKeyBinding.install(controller, viewerComponentPanel);
                        controller.getDocumentViewController().setAnnotationCallback(
                                new org.icepdf.ri.common.MyAnnotationCallback(
                                controller.getDocumentViewController()));
                        dialog.add(viewerComponentPanel);
                        controller.openDocument(path);
                }
                Dimension size = getFullScreen();
                dialog.setSize(size);
                dialog.setVisible(true);
        }

        public void undo() {}

        private Dimension getFullScreen() {
                Toolkit toolkit = Toolkit.getDefaultToolkit();
                Dimension screenSize = toolkit.getScreenSize();
                return screenSize;
        }
}