package mallemuck.model.command;
/**
 *
 */
public class FileTypeVerifier {

        public FileTypeVerifier() {}

        private static final String[] text = {
                ".txt",
                ".xml",
                ".log",
                ".java",
                ".properties",
                ".sh"
        };

        private static final String RTF = ".rtf";

        private static final String PDF = ".pdf";

        private static final String[] picture = {
                ".jpg", ".jpeg", ".png", ".gif"};

        public boolean isText(String name) {
                for (String type : text) {
                        if (name.endsWith(type)) {
                                return true;
                        }
                }
                return false;
        }

        public boolean isRTF(String name) {
                return name.endsWith(RTF);
        }

        public boolean isPDF(String name) {
                return name.endsWith(PDF);
        }

        public boolean isPicture(String name) {
                for (String type : picture) {
                        if (name.endsWith(type)) {
                                return true;
                        }
                }
                return false;
        }
}
