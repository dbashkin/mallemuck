package mallemuck.model.command;

import java.io.File;
import java.util.Collection;
import javax.swing.JOptionPane;
import org.runningman.mailutils.Credentials;
import org.runningman.mailutils.MessageData;
import org.runningman.mailutils.SMTPSender;

/**
 * Sends selected files via e-mail.
 */
public class SendEmail implements Command {
        public void perform(CommandData data) throws Exception {
                Collection<File> files =
                        (Collection<File>) data.getValue(Keys.SOURCE_FILES);
                Credentials credentials = (Credentials) data.getValue(Keys.CREDENTIALS);
                SMTPSender sender = new SMTPSender();
                sender.setCredentials(credentials);
                MessageData messageData = (MessageData) data.getValue(Keys.MESSAGE_DATA);
                messageData.addFiles(files);
                messageData.setSubject("Sending file.");
                messageData.setContent("");
                sender.send(messageData);
                JOptionPane.showMessageDialog(null, "Отправлено.");
        }

        public void undo() {
        }
}