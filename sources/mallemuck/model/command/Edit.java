package mallemuck.model.command;

import java.io.File;
import mallemuck.view.EditDialog;

/**
 *
 */
public class Edit implements Command {

    public void perform(CommandData data) throws Exception {
        File file = (File)data.getValue(Keys.FILE);
        if (file.isDirectory()) {
            return;
        }
        new EditDialog(file);
    }

    public void undo() {}
}
