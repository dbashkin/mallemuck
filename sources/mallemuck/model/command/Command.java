package mallemuck.model.command;

/**
 *
 */
public interface Command {
        public void perform(CommandData data) throws Exception;
        public void undo();
}