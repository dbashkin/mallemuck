package mallemuck.model;

/**
 * Contains button names.
 */
public interface Button {
        String SEND = "send";
        String CANSEL = "cansel";
}
