package mallemuck.model;

import java.awt.Font;
import java.awt.Image;
import java.net.URL;
import java.util.Collection;
import javax.swing.ImageIcon;
import javax.swing.LookAndFeel;
import javax.swing.UIDefaults;
import javax.swing.UIManager;
import javax.swing.plaf.FontUIResource;

/**
 * Provides access to resources.
 */
public class Resourses {
        public Resourses() {}
        public Font getDefaultFont() {
                return new FontUIResource("SansSerif", Font.BOLD, 14);
        }
        public Collection<Object> getUIPropertyKeys() {
                LookAndFeel looknfeel = UIManager.getLookAndFeel();
                UIDefaults defaults = looknfeel.getDefaults();
                return defaults.keySet();
        }
        public Image getLogo() {
                URL url = getClass().getResource("/icons/TB-7-1.jpg");
                return new ImageIcon(url).getImage();
        }
}