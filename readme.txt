File manager a-la Total Commander (two panel) written in JAVA. It contains:
   - ImageViewer(JPEG, PNG);
   - Text viewer;
   - Text editor;
   - PDF, RTF viewer;
   - Email sender.
This application needs JAVA 1.7 or higher.
To run the project from the command line type the following:
> java -jar mallemuck.jar

